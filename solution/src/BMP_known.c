#include "BMP_known.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static struct bmp_header create_header(uint64_t width, uint64_t height) {
    return (struct bmp_header) {
        .bfType = 0x4d42,
        .bfileSize = sizeof(struct bmp_header) + width * sizeof( struct pixel ) * height + sizeof( struct pixel ) * (width % 4),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = (uint32_t) width,
        .biHeight = (uint32_t) height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = height * width * sizeof(struct pixel) + (width % 4) * height,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

static enum bmp_read_status check_header (const struct bmp_header * header) {
    if (header->bfType != 0x4d42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biBitCount != 24) {
        return READ_INVALID_BITS;
    }
    if (header->biSize != 40) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

uint32_t get_padding (uint32_t width) {
    return (4 - (sizeof(struct pixel) * width) % 4);
}


enum bmp_read_status from_bmp(FILE* in, struct image* image) {
    struct bmp_header* header = malloc(sizeof (struct bmp_header));
    if (!header) {
        return READ_INVALID_HEADER;
    }

    fread(header, sizeof(struct bmp_header), 1, in);

    if (check_header(header) != READ_OK) {
        return check_header(header);
    }

    *image = create_image(header->biWidth, header->biHeight);

    free(header);

    for (uint32_t i = 0; i < image->height; i++) {
        if (fread(&(image->data[i*image->width]), sizeof(struct pixel), image->width, in) != image->width) {
            return READ_INVALID_BITS;
        }
        fseek(in, (int64_t)(image->width) % 4, SEEK_CUR);
    }
    return READ_OK;
}

enum bmp_write_status to_bmp( FILE* out, struct image const* image ) {

    struct bmp_header header = create_header(image->width, image->height);

    if (fwrite(&header, sizeof(struct  bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    const size_t zero_byte = 0;

    for (uint64_t i = 0; i < image->height; i++) {
        if (fwrite(&(image->data[i*image->width]), sizeof(struct pixel), image->width, out) != image->width) {
            return WRITE_ERROR;
        }
        if (fwrite(&zero_byte, sizeof(uint8_t), image->width % 4, out) != image->width % 4) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

