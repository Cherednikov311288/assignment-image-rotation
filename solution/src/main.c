#include <stdio.h>

#include "rotate.h"
#include "BMP_known.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3) {
        return 1;
    }

    FILE* in = {0};
    if (read_file(&in, argv[1]) != FILE_READ_OK) {
        return 2;
    }

    struct image source;
    if (from_bmp(in, &source) != READ_OK) {
        return 2;
    }

    if (close_file(in) != FILE_CLOSE_OK) {
        return 3;
    }

    struct image result = rotate(source);

    destroy_image(&source);

    FILE* out = {0};

    if (write_file(&out, argv[2]) != FILE_WRITE_OK) {
        return 4;
    }

    if (to_bmp(out, &result) != WRITE_OK) {
        return 4;
    }

    destroy_image(&result);

    if (close_file(out) != FILE_CLOSE_OK) {
        return 3;
    }

    return 0;
}
