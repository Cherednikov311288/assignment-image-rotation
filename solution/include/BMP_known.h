#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_KNOWN_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_KNOWN_H

#include <stdio.h>
#include <stdint.h>
#include "image_known.h"

enum bmp_read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum bmp_write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};


enum bmp_read_status  from_bmp(FILE* in, struct image* image);

enum bmp_write_status to_bmp( FILE* out, struct image const* image );

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_KNOWN_H
