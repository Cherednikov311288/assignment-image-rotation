#include "rotate.h"

struct image rotate( struct image const source ) {
    struct image out = create_image(source.height, source.width);
    for (int64_t i = 0; i < source.width; i++) {
        for (int64_t j = 0; j < source.height; j++) {
            out.data[i * out.width + j] = source.data[(source.height-1-j)*source.width+i];
        }
    }
    return out;
}
