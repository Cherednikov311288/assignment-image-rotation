#include "image_known.h"

struct image create_image(uint64_t width, uint64_t height) {
    struct image out = {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))
    };
    return out;
}

void destroy_image(struct image* image) {
    free(image->data);
}

enum read_file_status read_file (FILE** file, const char * path) {
    *file = fopen(path, "rb");
    if (file != NULL) {
        return FILE_READ_OK;
    } else {
        return FILE_READ_ERROR;
    }
}

enum write_file_status write_file (FILE** file, const char * path) {
    *file = fopen(path, "wb");
    if (file != NULL) {
        return FILE_WRITE_OK;
    } else {
        return FILE_WRITE_ERROR;
    }
}

enum close_file_status close_file (FILE* file) {
    if (file == NULL) {
        return FILE_CLOSE_ERROR;
    }
    fclose(file);
    return FILE_CLOSE_OK;
}
