//
// Created by Egor on 14.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_KNOWN_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_KNOWN_H

#include <stdint.h>
#include <malloc.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

enum read_file_status  {
    FILE_READ_OK = 0,
    FILE_READ_ERROR
};

enum read_file_status read_file (FILE** file, const char * path);

enum  write_file_status {
    FILE_WRITE_OK = 0,
    FILE_WRITE_ERROR
};

enum write_file_status write_file (FILE** file, const char * path);

enum close_file_status {
    FILE_CLOSE_OK = 0,
    FILE_CLOSE_ERROR
};

enum close_file_status close_file (FILE* file);

struct image create_image(uint64_t width, uint64_t height);

void destroy_image(struct image* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_KNOWN_H

